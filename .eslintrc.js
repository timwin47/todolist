module.exports = {
  root: true,
  extends: [
    '@react-native-community',
    'plugin:@typescript-eslint/recommended',
    'prettier/@typescript-eslint',
    'plugin:prettier/recommended'
  ],
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint'],
  rules: {
    "@typescript-eslint/no-explicit-any": "off",
    "@typescript-eslint/ban-ts-comment": "off",
    "react-hooks/exhaustive-deps": "off",
    "react/jsx-pascal-case": "error",
    "no-var": "error",
    "semi": "warn",
    "no-multiple-empty-lines": "error",
    "no-extra-semi": "error",
    "no-dupe-keys": "error",
    "no-dupe-args": "error",
    "no-console": "warn",
    "no-func-assign": "error",
    "no-self-assign": "error",
    "no-eval": "error",
    "eqeqeq": "error",
    "max-depth": ["error", 3],
    "prefer-destructuring": ["error", { "object": true, "array": false }]
  },
  settings: {
    'import/resolver': {
      node: {
        extensions: ['.js', '.jsx', '.ts', '.tsx', '.json']
      }
    }
  },
  env: {
    jest: true
  }
};