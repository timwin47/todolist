
import {
  SafeAreaView,
} from 'react-native';
import { PersistGate } from 'redux-persist/integration/react';
import {Provider} from 'react-redux';
import { store, persistor } from './services/redux/store';
import Navigation from './Navigation';

function App(): JSX.Element {
  return (
    <SafeAreaView style={{flex:1}}>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <Navigation />
        </PersistGate>
      </Provider>
    </SafeAreaView>
  );
}



export default App;
