import { useState } from 'react';
import { View, TextInput, Button } from 'react-native';
import globalStyles from '../../globalStyles'
import {taskApi} from '../../services'

type Props = {
  navigation: any
}
interface TodoParams {
  username: string
  email: string
  text: string
}

const AddTodo: React.FC<Props> = ({navigation}) => {
  const [taskParam, setTasksParam] = useState<TodoParams>({
    username: '',
    email: '',
    text: ''
  });
  const [loading, setLoading] = useState<boolean>(false);

  const handleAddTodo = ()=>{
    if(!loading) 
    taskApi.actionCreateTodo(taskParam, setLoading, navigation)
  }
  const handleChangeParam = (value: string, key: string)=>{
    setTasksParam(prevState => ({
      ...prevState,
      [key]: value
    }));
  }  
  return (
    <View style={globalStyles.page}>
      <TextInput
        value={taskParam.username}
        autoFocus={true}
        onChangeText={(value)=> handleChangeParam(value, "username")}
        placeholder="Имя"
        style={globalStyles.input}
      />
      
      <TextInput
        value={taskParam.email}
        onChangeText={(value)=> handleChangeParam(value, "email")}
        placeholder="Email"
        style={globalStyles.input}
      />
      <TextInput
        value={taskParam.text}
        onChangeText={(value)=> handleChangeParam(value, "text")}
        placeholder="Текст задачи"
        multiline
        numberOfLines={3}
        style={globalStyles.input}
      />
      <View style={globalStyles.block}>
        <Button
          onPress={handleAddTodo}
          title="Добавить"
          disabled={loading}
        />
      </View>
    </View>
  );
};

export default AddTodo;
