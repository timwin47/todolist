import React, { useState } from 'react';
import { View, TextInput, Button } from 'react-native';
import globalStyles from '../../globalStyles';
import {authApi} from '../../services';

interface Props {
  navigation: any
}

const Authentication:React.FC<Props> = ({navigation}) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);

  const handleAuth = () => {
    if(!loading) 
    authApi.actionAutharization(
      {username, password}, 
      setLoading, 
      navigation
    )
  };

  return (
    <View style={globalStyles.page}>
      <TextInput
        value={username}
        autoFocus={true}
        onChangeText={setUsername}
        placeholder="Имя"
        style={globalStyles.input}
      />

      <TextInput
        value={password}
        style={globalStyles.input}
        onChangeText={setPassword}
        placeholder="Пароль"

      />
      <View style={globalStyles.block}>
        <Button
          onPress={handleAuth}
          title="Авторизоваться"
          disabled={loading}
        />
      </View>

    </View>
  );
};

export default Authentication;
