import { useState, useEffect } from 'react';
import { FlatList, View } from 'react-native';
import {useSelector} from 'react-redux';
import {stateTasks, stateAuth, taskApi, TaskTypes} from '../../services';
import globalStyles from '../../globalStyles';
import {ItemTask, SelectCustom, Pagination} from '../../components';

type Props = {
  navigation: any
}

const sortField = [
  {value: "username", label: "По имени"},
  {value: "email", label: "По email"},
  {value: "status", label: "По статусу"},
  {value: "id", label: "По номеру"}
];

const sortDirection = [
  {value: "asc", label: "По возрастанию"},
  {value: "desc", label: "По убыванию"},
];
type SelectType = {
  value: string
  label: string
}
const TasksList: React.FC<Props> = ({navigation}) => {
  const {tasks, totalTaskCount} = useSelector(stateTasks);
  const {token} = useSelector(stateAuth);
  const [page, setPage] = useState(1);
  const [sortFieldTarget, setSortFieldTarget] = useState("id");
  const [sortDirectionTarget, setSortDirectionTarget] = useState("asc");
  const lengthList = tasks.length;

  useEffect(()=>{
    taskApi.actionGetTodos(page, sortFieldTarget, sortDirectionTarget)
  }, [page]);

  const openEditTask = (item: TaskTypes)=>{
    navigation.navigate('EditTask', item)
  }

  const renderItem = ({ item }:{item: TaskTypes}) => (
    <ItemTask item={item} token={token} openEditTask={openEditTask} />
  );

  const handleFieldSelected = (item: SelectType)=>{
    setSortFieldTarget(item.value);
    taskApi.actionGetTodos(page, item.value, sortFieldTarget);
  };

  const handleDirectionSelected = (item: SelectType)=>{
    setSortDirectionTarget(item.value);
    taskApi.actionGetTodos(page, sortDirectionTarget, item.value);
  };

  return (
    <FlatList
      data={tasks}
      ListHeaderComponent={
        <View style={globalStyles.rowBlock}>
          <SelectCustom
            placholder="Сортировка"
            list={sortField}
            handleSelected={handleFieldSelected}
          />
          <SelectCustom
            placholder="По возрастанию"
            list={sortDirection}
            handleSelected={handleDirectionSelected}
          />
        </View>
      }
      ListFooterComponent={
        totalTaskCount > lengthList ? 
        <Pagination 
          setPage={setPage}
          page={page}
          totalTaskCount={totalTaskCount}
          lengthList={lengthList}
        /> : null
      }
      style={globalStyles.page}
      renderItem={renderItem}
      keyExtractor={(item) => item.id.toString()}
    />
  );
};

export default TasksList;
