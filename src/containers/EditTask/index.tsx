import { useState } from 'react';
import { View, Text, TextInput, Button, } from 'react-native';
import SelectDropdown from 'react-native-select-dropdown'
import {useSelector} from 'react-redux';
import globalStyles from '../../globalStyles'
import {taskApi, stateAuth} from '../../services'

type Props = {
  navigation: any
  route: any
}
interface TodoParams {
  username: string
  email: string
  text: string
}
type SelectType = {
  value: number
  label: string
}
const statusList = [
  {value: 10, label: "Выполнено"},
  {value: 0, label: "Не выполнено"},
];
const EditTask: React.FC<Props> = ({navigation, route}) => {
  const {email, status, id, text, username} = route.params
  const {token} = useSelector(stateAuth);
  const [taskText, setTaskText] = useState<string>("");
  const [taskStatus, setTaskStatus] = useState(status)
  const [loading, setLoading] = useState<boolean>(false);

  const handleSaveTask = ()=>{
    const checkStatus = taskText && taskStatus >= 10 || status && taskStatus ? 
      11 
    : taskText && taskStatus === 0 || status && taskStatus === 0? 
      1
    : taskStatus;

    if(!loading) taskApi.actionEditTodo({
      id, 
      token,
      text: taskText ? taskText : text, 
      status: checkStatus
    }, setLoading, navigation)
  }
  const handleSelected = (target: SelectType)=>{
    setTaskStatus(target.value)
  }
  
  return (
    <View style={globalStyles.page}>
      <Text>Email: {email}</Text>
      <Text>Имя: {username}</Text>
      <Text>№ {id}</Text>

      <Text style={globalStyles.block}>Статус: ✏️</Text>
      <SelectDropdown
        data={statusList}
        defaultButtonText={
          status > 1 ?
            "Выполнено" 
          : "Не выполнено"
        }
        rowTextStyle={{fontSize: 14}}
        buttonTextStyle={{fontSize: 14}}
        buttonStyle={{padding: 5, borderRadius: 10}}
        onSelect={handleSelected}
        buttonTextAfterSelection={(selectedItem) => selectedItem.label}
        rowTextForSelection={(item) => item.label}
      />

      <Text style={globalStyles.block}>Задание: ✏️</Text>
      <TextInput
        defaultValue={text}
        onChangeText={setTaskText}
        placeholder="Текст задачи"
        multiline
        numberOfLines={4}
        style={globalStyles.input}
      /> 

      
      <View style={globalStyles.block}>
        <Button
          onPress={handleSaveTask}
          title="Сохранить"
          disabled={loading}
        />
      </View>
    </View>
  );
};

export default EditTask;
