import {ToastAndroid} from 'react-native';

export const validationEmail = (email: string | undefined)=>{
  if(email){
    const pattern = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
    
    if(pattern.test(email)){
      return true;
    }
    ToastAndroid.showWithGravity(
      "Не валидный емайл! Пример: examle@gmail.com",       
      ToastAndroid.LONG,
      ToastAndroid.CENTER,
    );
    return false
  }

  ToastAndroid.showWithGravity(
    "Введите емайл",       
    ToastAndroid.LONG,
    ToastAndroid.CENTER,
  );
  return false
}

export const inputIsEmpty = (fields: any): boolean => {
  let isEmpty = true
  for (const fieldName in fields) {
    const fieldValue = fields[fieldName].value;
    if (!fieldValue) {
      const errorMessage = fields[fieldName].message;
      ToastAndroid.show(`${errorMessage}`, ToastAndroid.LONG);
      return isEmpty = false;
    }
  }
  return isEmpty;
}