const colors = {
  mainColor: "#1677ff",
  grayDark: '#435360',
  grayMiddle: "#b2b2b2",
  grayLight: "#DADADA",
}


export {colors}