import {store} from '../../redux/store';

const dispatchAction = (action: any) => {
  store.dispatch(action);
};

export default dispatchAction