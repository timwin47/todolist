import {ToastAndroid} from 'react-native';
import fetchData from '../index';
import {developerName} from '../../../config';
import dispatchAction from './dispatchAction'
import {
  inputIsEmpty,
} from '../../utils/helpers';

type Login = {
  username: string
  password: string
}
const actionAutharization = async(
  {username, password}: Login, 
  setLoading: (status: boolean)=>void,
  navigation: any
) => {

  const fields = {
    username: {value: username, message: "Введите Имя"},
    password: {value: password, message: "Введите пароль"},
  }

  if(inputIsEmpty(fields)){
    setLoading(true)

    const {response} = await fetchData({
      url: `/login?developer=${developerName}`,
      body:{
        username,
        password,
      },
      method: 'POST',
    });

    if (response.status === "ok") {
      dispatchAction({type: "UPDATE_TOKEN", token: response.message.token})
      return navigation.goBack()
    }
    ToastAndroid.show(
      "Неверный логин или пароль",       
      ToastAndroid.LONG,
    );
    setLoading(false)
  }
}

const authApi = {
  actionAutharization,
};

export default authApi;