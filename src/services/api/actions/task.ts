import {ToastAndroid} from 'react-native';
import fetchData from '../index';
import dispatchAction from './dispatchAction';
import {developerName} from '../../../config'; 
import {
  inputIsEmpty,
  validationEmail
} from '../../utils/helpers';

type NewTask = {
  username: string
  email: string
  text: string
}
type editTask = {
  id: number
  text: string
  token: string
  status: number
}
const actionGetTodos = async(
  page: number, 
  sortField: string,
  sortDirection: string
) => {
  
  const {response} = await fetchData({
    url: `/?developer=${developerName}&sort_field=${sortField}&sort_direction=${sortDirection}&page=${page}`,
  });

  if(response.status === "ok") {
    return dispatchAction({
      type: "UPDATE_TASKS_LIST", 
      tasks: response.message.tasks,
      totalTaskCount: +response.message.total_task_count
    })
  }

};

const actionEditTodo = async(
  {id, text, token, status}: editTask,
  setLoading: (status: boolean)=> void,
  navigation: any
) => {
  const fields = {
    text: {value: text, message: "Заполните поле задачи"}
  };
  if(inputIsEmpty(fields)){
    setLoading(true);

    const {response} = await fetchData({
      url: `/edit/${id}?developer=${developerName}`,
      body: {
        text,
        token,
        status
      },
      method: "POST"
    });
  
    if(response.status === "ok") {
      dispatchAction({
        type: "EDIT_TASK", 
        param: {text, id, status}
      });
      ToastAndroid.show(
        "Изменения успешно сохранены",       
        ToastAndroid.LONG,
      );
      return navigation.goBack();
    }
    setLoading(false);
  }
};

const actionCreateTodo = async(
  {username, text, email}: NewTask,
  setLoading: (status: boolean)=> void,
  navigation: any
) => {
  const fields = {
    username: {value: username, message: "Введите Имя"},
    text: {value: text, message: "Введите текст задачи"},
  }

  if(validationEmail(email) && inputIsEmpty(fields)){
    setLoading(true)
    const {response} = await fetchData({
      url: `/create?developer=${developerName}`,
      body:{
        username, 
        text, 
        email
      },
      method: "POST"
    });
   
    if(response?.status === "ok") {
      ToastAndroid.show(
        "Задача успешнео добавлена",       
        ToastAndroid.LONG,
      );
      dispatchAction({type: "ADD_TODO_IN_LIST", task: response.message})
      return navigation.goBack()
    }
    setLoading(false);
  }
};

const todosApi = {
  actionGetTodos,
  actionEditTodo,
  actionCreateTodo
};

export default todosApi;