import axios from "axios";
import {baseUrl} from '../../config';

const headers = {
	"Content-Type": "multipart/form-data",
};

const axiosInstance = axios.create({
  baseURL: baseUrl,
	headers: headers,
});

export default axiosInstance;
