import {ToastAndroid} from 'react-native';
import axios from './axios';

interface FetchDataOptions {
  method?: string | undefined;
  url: string;
  body?: any;
}

export const fetchData = async ({ method = 'GET', url, body }: FetchDataOptions): Promise<any> => {
  try {
    const response = await axios({
      method,
      url,
      data: body,
    });
    //console.log('RESPONSE_DATA:', `URL: ${url}`, response);

    return {status: response.status, response: response.data};
  } catch (error: any) {
    ToastAndroid.show(`${error.message}`, ToastAndroid.LONG);
    //console.log('Error:', `URL: ${url}`, error);
    return {status: error.status, response: error.response?.data}
  }
};

export default fetchData