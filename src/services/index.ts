import {authApi, taskApi} from './api/actions';
import {stateTasks, stateAuth} from "./redux/selectors";
import {TaskTypes} from './redux/types';
import {colors} from './utils/constants';

export {
  authApi, stateTasks,
  taskApi, stateAuth, colors
};
export type { TaskTypes };
