export type TaskTypes = {
  id: number;
  email: string;
  status: number;
  text: string;
  username: string;
}
