import { RootState } from './reducers';

export const stateTasks = (state: RootState) => state.tasksReducer;
export const stateAuth = (state: RootState) => state.authReducer;