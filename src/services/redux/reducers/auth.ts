import { Action } from 'redux';
import {AuthKey} from '../reducerKeys';

interface AuthReducerState {
  token: string;
}

const initialState: AuthReducerState = {
  token: '',
};

interface UpdateTokenAction extends Action<AuthKey.UPDATE_TOKEN> {
  token: string;
}

type AuthReducerActions = UpdateTokenAction;

const authReducer = (state = initialState, action: AuthReducerActions): AuthReducerState => {
  switch (action.type) {
    case AuthKey.UPDATE_TOKEN:
      return {
        ...state,
        token: action.token,
      };

    default:
      return state;
  }
};

export default authReducer;
