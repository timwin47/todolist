// reducers/index.ts;
import { combineReducers } from 'redux';
import tasksReducer from './tasks'
import authReducer from './auth'

const rootReducer = combineReducers({
  tasksReducer,
  authReducer
});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
