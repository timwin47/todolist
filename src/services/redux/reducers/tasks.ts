import { Action } from 'redux';
import {TasksKey} from '../reducerKeys';
import {TaskTypes} from '../types';

interface TasksReducerState {
  tasks: TaskTypes[];
  totalTaskCount: number;
}

const initialState: TasksReducerState = {
  tasks: [],
  totalTaskCount: 0
};

interface AddTaskAction extends Action<TasksKey.ADD_TODO_IN_LIST> {
  task: TaskTypes;
}
interface UpdateTasksAction extends Action<TasksKey.UPDATE_TASKS_LIST> {
  tasks: TaskTypes[];
  totalTaskCount: number
}
interface EditTaskAction extends Action<TasksKey.EDIT_TASK> {
  param: {text: string, id: number, status: number}
}

type TasksReducerActions = AddTaskAction | EditTaskAction | UpdateTasksAction;

const tasksReducer = (state = initialState, action: TasksReducerActions): TasksReducerState => {
  switch (action.type) {
    case TasksKey.ADD_TODO_IN_LIST:
      return {
        ...state,
        totalTaskCount: +state.totalTaskCount+1,
        tasks: state.totalTaskCount < 3 ? [...state.tasks, action.task] : state.tasks,
      };

    case TasksKey.UPDATE_TASKS_LIST:
      return {
        ...state,
        tasks: action.tasks,
        totalTaskCount: action.totalTaskCount
      };

    case TasksKey.EDIT_TASK:
      return {
        ...state,
        tasks: state.tasks.map((item)=>{
          if(item.id === action.param.id){
            return {
              ...item, 
              status: action.param.status, 
              text: action.param.text
            }
          }
          return item
        })
      };
    default:
      return state;
  }
};

export default tasksReducer;
