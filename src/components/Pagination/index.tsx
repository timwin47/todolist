import { memo, useState, useEffect } from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import styles from './style';
import {colors} from '../../services';

type Props = {
  totalTaskCount: number
  lengthList: number
  page: number
  setPage: (page: number)=>void
}

const Pagination: React.FC<Props> = ({
  totalTaskCount,
  setPage,
  page,
  lengthList,
}) => {
  const [paginatesList, setPaginatesList] = useState<number[]>([]);

  useEffect(()=>{
    const list = []
    const length = Math.ceil(totalTaskCount / lengthList)
    for (let i = 1; i <= length; i++){
      list.push(i)
    }
    setPaginatesList(list)
  }, [totalTaskCount]);

  const handleNavigate = (newPage: number) => {
    setPage(newPage)
  };
  
  return (
    <View style={styles.paginateBlock}>
      {
        paginatesList.map((item)=>(
          <TouchableOpacity 
            key={item}
            style={{
              ...styles.paginateItem, 
              backgroundColor: item === page ? colors.mainColor : colors.grayMiddle
            }}
            onPress={()=>handleNavigate(item)}
          >
            <Text style={styles.textItem}>{item}</Text>
          </TouchableOpacity>
        ))
      }
    </View>
  );
};


export default memo(Pagination);
