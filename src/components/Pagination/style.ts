import { StyleSheet } from 'react-native';
import {colors} from '../../services'

const styles = StyleSheet.create({
  paginateBlock: {
    paddingVertical: 10,
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  paginateItem: {
    paddingVertical: 5,
    paddingHorizontal: 10,
    margin: 5,
    borderRadius: 5,
  },
  textItem: {
    fontSize: 18,
    color: '#fff',
  }
})

export default styles