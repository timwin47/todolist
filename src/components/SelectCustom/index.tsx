import SelectDropdown from 'react-native-select-dropdown'

type SelectType = {
  value: string
  label: string
}

interface Props {
  handleSelected: (item: SelectType)=>void
  placholder: string
  list: SelectType[]
}

const SelectCustom: React.FC<Props> = ({handleSelected, list, placholder})=>{
  return(
    <SelectDropdown
      data={list}
      defaultButtonText={placholder}
      rowTextStyle={{fontSize: 14}}
      buttonTextStyle={{fontSize: 14}}
      buttonStyle={{flex: 1, borderRadius: 10}}
      onSelect={handleSelected}
      buttonTextAfterSelection={(selectedItem) => selectedItem.label}
      rowTextForSelection={(item) => item.label}
    />
  );
};

export default SelectCustom;