import { useState } from 'react';
import { View, TouchableOpacity, TouchableWithoutFeedback, Text } from 'react-native';
import styles from './style';
import globalStyles from '../../globalStyles';
import {TaskTypes} from '../../services';

type Props = {
  item: TaskTypes
  token: string
  openEditTask: (item: TaskTypes)=> void
}

const ItemTask: React.FC<Props> = ({item, openEditTask, token}) => {
  const [moreText, setMoreText] = useState(false)

  return (
    <View style={styles.itemBlock}>
      <TouchableWithoutFeedback onPress={()=>setMoreText(!moreText)}>
        <View>
          <View style={globalStyles.rowBetweenBlock}>
            <Text>Имя: {item.username}</Text>
            <Text>№{item.id}</Text>
          </View>
          <Text>Email: {item.email}</Text>
          <View>
            <View style={globalStyles.rowBlock}>
              <Text>Статус:</Text>
              {
                item.status > 1 ? 
                  <Text style={styles.statusComplit}>Выполнено</Text> 
                : <Text style={styles.status}>Не выполнено</Text> 
              }
            </View>
            {item.status === 11 || item.status === 1 ? 
              <Text>📝Отредактировано администратором</Text>
            :null
            }
            <Text 
              style={styles.taskText} 
              numberOfLines={moreText ? 30 : 1}
            >
              Задача: {item.text}
            </Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
      {token ? 
        <TouchableOpacity onPress={()=>openEditTask(item)}>
          <Text style={styles.textEdit}>✍️ Редактировать</Text>
        </TouchableOpacity> 
      :null}

    </View>
  );
};


export default ItemTask;
