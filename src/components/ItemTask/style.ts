import { StyleSheet } from 'react-native';
import {colors} from '../../services'

const styles = StyleSheet.create({
  itemBlock: {
    padding: 10,
    marginTop: 16,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: colors.mainColor
  },
  taskText: {
    fontWeight: 'bold',
  },
  statusComplit: {
    color: 'green'
  },
  textEdit:{
    textAlign: 'center',
    borderWidth: 1,
    padding: 5,
    borderRadius: 7,
    marginTop: 5,
    borderStyle: 'dotted',
  },
  status: {
    color: colors.mainColor
  },
})

export default styles