import { StyleSheet } from 'react-native';
import {colors} from '../services'

const styles = StyleSheet.create({
  input: {
    padding: 5,
    width: '100%',
    marginVertical: 10,
    borderBottomWidth: 1, 
    borderColor: '#e4e4e4'
  },
  inputTask: {
    padding: 5,
    flex: 1,
  },
  rowBlock: {
    flexDirection: 'row',
    gap: 10,
    alignItems: 'center',
  },
  rowBetweenBlock: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  title: {
    fontSize: 20
  },
  page: {
    padding: 16,
    width: '100%',
    minHeight: 400,
    flex:1,
    backgroundColor: '#fff',
  }, 
  block: {
    marginTop: 16,
    width: '100%'
  }
});

export default styles