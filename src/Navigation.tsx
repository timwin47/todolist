import {
  Text,
  TouchableOpacity,
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import {stateAuth} from './services'
import {useDispatch, useSelector} from 'react-redux';
import {TasksList, EditTask, Authentication, AddTask} from './containers'
import globalStyle from './globalStyles'

const Stack = createNativeStackNavigator();
const defaultOptions: any = {
  headerTitleAlign: 'center',
  headerTitleStyle: { fontWeight: 'bold'},
  tabBarInactiveTintColor: 'gray',
}

function Navigation(): JSX.Element {
  const dispatch = useDispatch()
  const {token} = useSelector(stateAuth);

  const logout = ()=>{
    dispatch({type: "UPDATE_TOKEN", token: ''})
  }
  
  return (
    <NavigationContainer>  
      <Stack.Navigator initialRouteName="TaskList">
        <Stack.Screen 
          name="TaskList" 
          component={TasksList} 
          options={({ navigation }) => ({
            ...defaultOptions,
            headerBackVisible: false,
            headerLeft: () => (
              <TouchableOpacity onPress={()=>navigation.navigate('AddTask')}>
                <Text>➕</Text>
              </TouchableOpacity>
            ),
            headerTitle:()=><Text style={globalStyle.title}>Список задач</Text>,
            headerRight: () => token ?
              <TouchableOpacity onPress={logout}>
                <Text>🚪🚶</Text>
              </TouchableOpacity>
            :
              <TouchableOpacity 
                onPress={()=>navigation.navigate('Authentication')}
              >
                <Text>🔑</Text>
              </TouchableOpacity>
            ,
          })}
        />
        <Stack.Screen 
          name="AddTask" 
          component={AddTask} 
          options={{
            ...defaultOptions,
            headerTitle:()=><Text style={globalStyle.title}>Добавить задачу</Text>,
          }}
        />
        <Stack.Screen 
          name="Authentication" 
          component={Authentication} 
          options={{
            ...defaultOptions,
            headerTitle:()=><Text style={globalStyle.title}>Авторизация</Text>,
          }}
        />
        <Stack.Screen 
          name="EditTask" 
          component={EditTask} 
          options={{
            ...defaultOptions,
            headerTitle:()=><Text style={globalStyle.title}>Редактирование</Text>,
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}



export default Navigation;
